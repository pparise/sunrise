=======
Sunrise
=======


.. image:: https://img.shields.io/pypi/v/sunrise.svg
        :target: https://pypi.python.org/pypi/sunrise

.. image:: https://img.shields.io/travis/pparise/sunrise.svg
        :target: https://travis-ci.org/pparise/sunrise

.. image:: https://readthedocs.org/projects/sunrise/badge/?version=latest
        :target: https://sunrise.readthedocs.io/en/latest/?badge=latest
        :alt: Documentation Status



*Get Sunrise and Sunset Times*


Sunrise
--------
**Sunrise** is a command line tool to lookup sunrise and sunset times
from a web API.

* Free software: MIT license


Features
--------

* Short view with date, sunrise, and sunset times.
* Long view additional  civil, nautical, and astronomical times.
* Cache results locally to reduce API calls.

Credits
-------

`Sunrise Sunset.org`_ which provides a free API for sunset and sunrise times
for a given latitude and longitude.

.. _`Sunrise Sunset.org`: https://sunrise-sunset.org/api
