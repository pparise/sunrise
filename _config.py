#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""Work with confuguration files (aka ini files).

Todo:
    - Allow update of configuration values
    - Use a more standard default location

"""

import os
import configparser

# configuration file name
INIFILE = os.path.join(os.path.dirname(__file__), 'sunrise.ini')

# default values when file needs to be created
DEFAULT_LOCATION = {
    'Latitude': '45.702259',
    'Longitude': '-73.634628',
    'City': 'Terrebonne',
    'State': 'Québec',
    'Country': 'Canada'
}

def get_config():
    """Read configuration information from ini file"""
    # if config exists, read it and return as a dict
    if os.path.exists(INIFILE):
        location = read_config()
    # if not create it and return defaults as a dict
    else:
        location = create_config()
    return location


def read_config():
    """Read config file."""
    config = configparser.ConfigParser()
    config.read(INIFILE)
    geolocation = config.items('Geolocation')
    return dict(geolocation)


def write_config():
    """Save config file."""


def create_config():
    """Create a default config file."""
    config = configparser.ConfigParser()
    config['Geolocation'] = DEFAULT_LOCATION
    with open(INIFILE, 'w') as configfile:
        config.write(configfile)
    return dict(DEFAULT_LOCATION)


if __name__ == '__main__':
    get_config()
