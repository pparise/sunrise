#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""Get Sunrise and Sunset Times.

Description:
    Print detailed surise and sunset times to the console.
    Times are retrieved from a web API based on geolocation
    coordinates. To limit API requests, the results are saved
    and re-used if the same day is requested again.

    API: https://sunrise-sunset.org/api

Optional Arguments:
    -h --help           Show this help screen and exit.
    -v --version        Show version.
    -s --short          Print short version.

Notes:
    - Times are converted from UTC to local time.
    - Local time is rounded to the nearest minute.
    - Geolocation configuration is read from the 'sunrise.ini' file.
    - Location defaults to Terrebonne (45.702259, -73.634628)

Todo:
    - Add an optional date argument to lookup any date
    - Allow configuration changes using reverse geocoding
"""

__author__ = "pparise"
__version__ = "1.0.0"
__license__ = "MIT"

# standard library modules
import os
import sys
import datetime
import json
import argparse

# 3rd party modules
import requests
import tzlocal

# local modules
sys.path.insert(0, os.path.abspath('..'))
import _config as config


def main(args):
    """Main entry point of the app"""

    # if the namespace help attribute exists, then show help (the module doctring)
    if hasattr(args, 'help'):
        print(__doc__)
        sys.exit(1)

    # if the short attribute exists, then display short version
    if args.short:
        short_version = True
    else:
        short_version = False

    # the date
    date = datetime.date.today()

    # get configuration values into dictionary
    location = {}
    location['Date'] = str(date)
    location.update(config.get_config())
    location['UTC Offset'] = str(get_offset_hours())

    # the file name identifier to use
    f = str(date) + '+lat_' + str(location['latitude']) + '+lng_' + str(location['longitude'])

    # the url query string to use
    query = f.replace('_', '=').replace('+', '&')

    # file name to save daily data
    filename = os.path.join(os.path.dirname(__file__), 'data/' + f + '.json')

    # get the data
    data = get_data(filename, query)

    # get API response status (is available in both cases)
    status = data['status']

    # display results
    if status == 'OK':
        print_results(date, location, data, short_version)
    # there was an error
    else:
        error_message('OOps: Something went wrong', data['status'])


def get_data(filename, query):
    """Get data from API or saved file."""

    # if the file has already been created today, then use the data from the file
    if os.path.exists(filename):

        # open file in read mode
        with open(filename, mode='r', encoding='UTF-8') as file:

            # load JSON data file into a dictionary
            data = json.load(file)

    # if the file does not exist, then get the data from the API call
    else:

        # build url for sunset and sunrise times API url
        url = 'https://api.sunrise-sunset.org/json?date=' + query

        # try to get data, check status of request, and handle exceptions
        try:
            response = requests.get(url, timeout=20)
            response.raise_for_status()
        except requests.exceptions.HTTPError as errh:
            error_message('Http Error', errh)
            sys.exit(1)
        except requests.exceptions.ConnectionError as errc:
            error_message('Connection Error', errc)
            sys.exit(1)
        except requests.exceptions.Timeout as errt:
            error_message('Timeout Error', errt)
            sys.exit(1)
        except requests.exceptions.RequestException as err:
            error_message('OOps: Something went wrong', err)
            sys.exit(1)

        # load the JSON response into a dictionary
        data = json.loads(response.text)

        # save the response to a new file so we don't repeat API calls
        with open(filename, mode='w', encoding='UTF-8') as file:

            # dump dictionary into file as a valid JSON string
            json.dump(data, file, ensure_ascii=False, indent=4)

    # return the json data
    return data


def print_results(date, location, data, short):
    """Print API call results to the screen."""

    # print the short version (date, sunrise and sunset only)
    if short is True:
        today = location['location'] + ': ' + str(date)
        sunrise = 'Sunrise: ' + format_time(date, convert_to_local(data['results']['sunrise']))
        sunset = 'Sunset: ' + format_time(date, convert_to_local(data['results']['sunset']))
        print('', today, '\033[93m│\033[m', sunrise, '\033[93m│\033[m', sunset)

    # print the long version (full results with borders)
    else:

        # maximum width
        width = 40

        # header
        print("\n\033[1m\033[93m Sunrise & Sunset Times\033[0m")

        # box top
        print("\033[93m┌" + "─" * width + "┐\033[0m")

        # print configuration values
        for k, v in location.items():
            key = k.title() + ':'
            value = v
            spaces = ' ' * (width - (len(k) + len(v) + 7))
            print("\033[93m│\033[m", key, spaces, "  " + value, "\033[93m│\033[m")

        # box splitter
        print("\033[93m├" + "─" * width + "┤\033[0m")

        # print all values
        for item in data['results']:
            if item == 'day_length':
                key = item.replace('_', ' ').title() + ':'
                value = data['results'][item][:5]
                spaces = ' ' * (width - (len(key) + len(value) + 4))
                print("\033[93m│\033[m", key, spaces, value, "\033[93m│\033[m")
            else:
                key = item.replace('_', ' ').title() + ':'
                value = format_time(date, convert_to_local(data['results'][item]))
                spaces = ' ' * (width - (len(key) + len(value) + 4))
                print("\033[93m│\033[m", key, spaces, value, "\033[93m│\033[m")

        # box bottom
        print("\033[93m└" + "─" * width + "┘\033[0m")

        # add a blank line
        print("")


def error_message(msg, err):
    """Display all errors in a consistant way."""
    print("\n\033[93mSunrise & Sunset Times \033[31mError!\033[0m")
    print("=" * 27)
    print(msg, err)
    print("")
    sys.exit(1)


def get_offset_hours():
    """Return the number of offset hours from UTC."""
    tz = tzlocal.get_localzone()
    d = datetime.datetime.now(tz)
    offset = d.utcoffset().total_seconds() / 60 / 60
    return offset


def convert_to_local(time):
    """Convert UTC time to local time and displays in military (24-hour) format."""
    offset = get_offset_hours()
    in_time = datetime.datetime.strptime(time, '%I:%M:%S %p') + datetime.timedelta(hours=offset)
    return datetime.datetime.strftime(in_time, '%H:%M:%S')


def format_time(date, time):
    """Format time."""
    time1 = '{0} {1}'.format(date, time)
    time2 = datetime.datetime.strptime(str(time1), '%Y-%m-%d %H:%M:%S')
    time3 = round_time(time2)
    return datetime.datetime.strftime(time3, '%H:%M')


def round_time(dt=None, round_to=60):
    """Round time the nearest minute."""
    if dt is None:
        dt = datetime.datetime.now()
    seconds = (dt - dt.min).seconds
    rounding = (seconds + round_to / 2) // round_to * round_to
    return dt + datetime.timedelta(0, rounding - seconds, -dt.microsecond)

def generate_argparser():
    """Create and return an argument parser object."""

    # create a ArgumentParser object instance and set the instance attributes
    parser = argparse.ArgumentParser(
        description='Sunrise-Sunset',
        epilog="That's all folks!",
        formatter_class=lambda prog: argparse.HelpFormatter(prog, max_help_position=36),
        add_help=False,
        allow_abbrev=False)

    # add argument for help
    parser.add_argument(
        '-h',
        '--help',
        action='store_true',
        dest='help',
        default=argparse.SUPPRESS)

    # add argument for version
    parser.add_argument(
        "-v",
        "--version",
        action="version",
        version="%(prog)s (version {version})".format(version=__version__))

    # optional short argument
    parser.add_argument(
        '-s',
        '--short',
        action='store_true',
        dest='short')

    return parser


if __name__ == '__main__':
    parser = generate_argparser()
    args = parser.parse_args()
    main(args)
